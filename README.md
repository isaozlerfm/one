# Sample Code for One👟
This code is the intellectual property of VodafoneZiggo and MonitorX. Therefor should in no circumstances be used for other purposes than the code review process.

# Vodafone

## Advice Tool Code (Vue.js) [(vodafone.nl/zakelijk/keuzehulp)](https://www.vodafone.nl/zakelijk/keuzehulp/#/)

*Responsible for: User Experience Design and Development*

```
> VFZ-demo--AT.zip
```

## Multiline Retention (Vue.js)

Unfortunately, this is in the private section (vodafone.nl/my) of the website. Therefore not able to demo a live version.

*Responsible for: User Experience Design (partly) and Development*

```
> VFZ-demo--MLR.zip
```

# Monitor X

## Dashboard (Vue.js / D3.js)

With this dashboard, you can select specific dates. Show overview data. Click through to see details (second selection on the bar chart bar). The development is still in progress. But the most important functionalities are working. With this dashboard, four different chart components are linked to each other. All charts are synced with each other. Interaction with one will also affect others, such as selecting, zooming, and panning.

*Responsible for: User Experience Design, Interface Design and Development*

```
> MNX-demo--Dashboard.zip

npm install
npm run serve

go to http://localhost:8082/
```
### **Default State**
![Default State](MNX-demo--dashboard-screens/dashboard:index:index.jpg)

### **Single Sensor State**
![Single Sensor State](MNX-demo--dashboard-screens/dashboard:index:single-sensor.jpg)

### **Single Selection Active State**
![Single Selection Active State](MNX-demo--dashboard-screens/dashboard:index:hover-state.jpg)

### **Sticky Footer State**
![Sticky Footer State](MNX-demo--dashboard-screens/dashboard:index:footer-state.jpg)